# Travertine

Travertine is a banded, more compact form of Limestone, characterised by the many holes and chambers through the stone. It varies in colour from dusty white to silver-grey, to gold, coral red and toffee.